# Description

- ERC20Permit implementation (with tests)
- deploy on ether/bsc scan to with latest compiler version to check verification works well

Remark: need to test frontend implementation

## Deployed contracts

- Compiler 0.8.3 (deployed with index = 0, 0x...319b):
  - Staker: 0xd50df372C0E01acB1B3f97375D611770d443f762
  - VCT: 0x85f46a96976d749177EE2C782b1D2A9D314EC8F9
  - ETH/VCT Uniswap pool: 0xB5F49567EcA974002256FD7C02b2caFa6f210fe7
  - proof of look: [unicrypt](https://www.unicrypt.network/amm/uniswap-kovan/pair/0xB5F49567EcA974002256FD7C02b2caFa6f210fe7)

## Unicrypt locker
Use [this address](https://kovan.unicrypt.network/locker) to lock LP tokens on kovan
