// Created contracts
const Staker = artifacts.require("Staker");
const VCT = artifacts.require("VCT");
//Module loading
require("chai").use(require("chai-as-promised")).should();
const truffleAssert = require("truffle-assertions");
const eth_const = require("@ethersproject/constants");
const eth_util = require("ethereumjs-util");
const Utils = require("./Utils.js");

contract("VCT", (accounts) => {
  let vct, staker;

  before(async () => {
    // Load Contracts
    staker = await Staker.new();
    vct = await VCT.new();
  });

  describe("VCT sending", async () => {
    it("FAILS: VCT not permitted, no sending possible", async () => {
      await truffleAssert.fails(staker.stake(Utils.toWei(100)));
    });

    // FIXME: test give up, tries on Kovan directly
    it.skip("permit", async () => {
      const nonce = await vct.nonces(accounts[0]);
      const deadline = eth_const.MaxUnit256;
      const amount = Utils.toWei(100);

      const digest = await getApprovalDigest(
        token,
        { owner: wallet.address, spender: other.address, value: TEST_AMOUNT },
        nonce,
        deadline
      );

      const { v, r, s } = eth_util.ecsign(
        Buffer.from(digest.slice(2), "hex"),
        Buffer.from(wallet.privateKey.slice(2), "hex")
      );

      vct.permit(
        accounts[0],
        staker.address,
        amount,
        deadline,
        v,
        hexlify(r),
        hexlify(s)
      );

      await truffleAssert.passes(staker.stake(vct.address, amount));

      let VCTbalance = await vct.balanceOf(staker.address);
      VCTbalance = parseFloat(Utils.fromWei(VCTbalance));
      expect(VCTbalance).to.be.eq(100);
    });
  });
});
