// SPDX-License-Identifier: MIT
pragma solidity ^0.8.1;

import "./VCT.sol";

contract Staker {
    function stake(VCT vct, uint256 amount) public {
        vct.transferFrom(msg.sender, address(this), amount);
    }
}
