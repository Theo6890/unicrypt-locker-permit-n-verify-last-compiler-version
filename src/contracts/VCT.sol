// SPDX-License-Identifier: MIT
pragma solidity ^0.8.1;

import "@openzeppelin/contracts/token/ERC20/extensions/draft-ERC20Permit.sol";

contract VCT is ERC20Permit {
    modifier onlyIfTotalSupplyNotSet() {
        require(totalSupply() == 0, "Total supply already set");
        _;
    }

    constructor()
        ERC20Permit("Verify Compiler Token")
        ERC20("Verify Compiler Token", "VCT")
    {
        _mint(_msgSender(), 10**7 * 10**18); // mint 10M to msg.sender
    }

    function _mint(address account, uint256 amount)
        internal
        override
        onlyIfTotalSupplyNotSet
    {
        super._mint(account, amount);
    }
}
